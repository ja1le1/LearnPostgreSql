初来乍到数据库
=============

## 知识点

* psql的基础
* 数据库简单操作
* 写个SQL

## 实战演习

~~~bash
$ sudo su postgres
$ psql --version
$ psql -l
$ createdb komablog
$ psql -l
$ psql komablog
> help
> \h
> \?
> \l
> \q
$ psql komablog
> select now();
> select version();
> \q
$ dropdb komablog
$ psql -l
~~~

## 课程文件

https://gitee.com/komavideo/LearnPostgreSql

## 小马视频频道

http://komavideo.com

## 复习
- 建数据库在数据库命令端之外
    - postgres@db-1:/home/g812126839qq$ 

      ```bash 
      sudo su postgres
      psql -l
      createdb db7.6
      psql db7.6
      psql -l
      \dt
      create table test1(title varchara(255), content text);
      \d 
      ```
    
      