操作数据表
=========

## 知识点

* create table
* drop table
* psql使用

## 实战演习

~~~bash
$ sudo su postgres
$ createdb komablog
$ psql -l
$ psql komablog
> create table posts (title varchar(255), content text);
> \dt
> \d posts
> alter table posts rename to komaposts;
> \dt
> drop table komaposts;
> \dt
> \q
$ nano db.sql
...
create table posts (title varchar(255), content text);
...
$ psql komablog
> \i db.sql
> \dt
~~~

## 注意
- 在postgresql 9.6版本 我尝试的时候不能在数据库端口？下新建sql文件只能在数据库外？（不是很专业的说法）

## 课程文件

https://gitee.com/komavideo/LearnPostgreSql

## 小马视频频道

http://komavideo.com
